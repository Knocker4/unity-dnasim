﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FullSpiralRotation : MonoBehaviour {

	public int rotationSpeed = 10;
//	public float breakUpSpeed = 0.005f;

//	public GameObject leftSide;
//	public GameObject rightSide;

//	[SerializeField] private float distance = 0.4f;
//	[SerializeField] private Vector3 _destLeft;
//	[SerializeField] private Vector3 _destRight;

//	private Boolean _breakingUp = false;

	// Use this for initialization
	void Start () {
		this.GetComponent<Rigidbody>().isKinematic = true;
//		_destLeft = new Vector3 (
//			leftSide.transform.localPosition.x, 
//			leftSide.transform.localPosition.y + distance / 2, 
//			leftSide.transform.localPosition.z);
//
//		_destRight = new Vector3 (
//			rightSide.transform.localPosition.x, 
//			rightSide.transform.localPosition.y - distance / 2, 
//			rightSide.transform.localPosition.z);
		
	}


	// Update is called once per frame
	void FixedUpdate () {
		var rotation = new Vector3 (0.0f, 1.0f, 0.0f) * rotationSpeed * Time.deltaTime;
		transform.Rotate(rotation);
//		if (_breakingUp) {
//			BreakUp ();
//		}
	}

//	void BreakUp() {
//		leftSide.transform.localPosition = Vector3.MoveTowards (
//			leftSide.transform.localPosition, 
//			_destLeft,
//			breakUpSpeed);
//		
//		rightSide.transform.localPosition = Vector3.MoveTowards (
//			rightSide.transform.localPosition, 
//			_destRight, 
//			breakUpSpeed);
////		rightSide.transform.Translate (new Vector3(0.0f, -1.0f, 0.0f));
//	}

}
