﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InsertSpiral : MonoBehaviour {

	// 0.692, -0.083, 0.562 - targetpos
	// 1.4, 1.4, 1.4 - targetscale

	public float descendingSpeed = 0.005f;
	public float growSpeed = 0.02f;

	public Boolean _shouldAnimate = false;
//	public LightsOnOff lightsScript;

	private Vector3 _dest;
	private Vector3 _targetSize;
	private float _distance = 0.5f;
	private float _size = 2f;
	private Boolean playHalo = true;
	private Color targetColor;
	public Renderer colorRenderer;


	public GameObject haloSource;
	public ParticleSystem particles;

	void Start() {
		_dest = new Vector3(
			this.transform.position.x,
			this.transform.position.y,
			this.transform.position.z
		);
		this.transform.position = new Vector3(
			this.transform.position.x,
			this.transform.position.y + _distance,
			this.transform.position.z
		);
		_targetSize = Vector3.one * _size;
		this.transform.localScale = Vector3.zero;
		targetColor = new Color (0.69f, 0.0f, 0.0f);
	}

	// Use this for initialization
	void Update () {
//		if (Input.GetKeyDown (KeyCode.Space)) {
//			_shouldAnimate = true;
//		}
	}

	public void ShouldAnimate(Boolean shouldAnimate) {
		_shouldAnimate = shouldAnimate;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (_shouldAnimate) {

			if (!this.transform.localScale.Equals (_targetSize)) {
				this.transform.localScale = Vector3.MoveTowards (
					this.transform.localScale,
					Vector3.one * _size,
					growSpeed
				);
			} else {
				this.transform.localPosition = Vector3.MoveTowards (
					this.transform.localPosition, 
					_dest,
					descendingSpeed
				);
				if (this.transform.localPosition.Equals (_dest)) {
					if (playHalo) {
						haloSource.GetComponent<Animator> ().SetTrigger ("HaloTrigger" );
//						particles.Play ();
						playHalo = false;
					}

					colorRenderer.material.color = Color.Lerp (
						colorRenderer.material.color,
						targetColor,
						growSpeed /// 2
					);
				}
			}
		}
	}
}
