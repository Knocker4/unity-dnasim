﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightOn : MonoBehaviour {

	public float lightSpeed = 0.005f;

//	private float finalIntensity = 100f;
//	private Light light;
	private float currentAlpha = 0f;
	private float finalAlpha = 1f;

	private Image img;

	// Use this for initialization
	void Start () {
//		light = GetComponent<Light> ();
		img = GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (currentAlpha < finalAlpha) {
			currentAlpha += lightSpeed;
			img.color = new Color(1f, 1f, 0.91f, currentAlpha);
		}
	}
}
