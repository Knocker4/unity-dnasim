﻿using System;
using UnityEngine;

public class BreakUp : MonoBehaviour {

	public float breakUpSpeed = 0.005f;

	public GameObject leftSide;
	public GameObject rightSide;

	[SerializeField] private float distance = 0.4f;
	[SerializeField] private Vector3 _destLeft;
	[SerializeField] private Vector3 _destRight;


	private Boolean _breakingUp = false;


	void Start () {
		this.GetComponent<Rigidbody>().isKinematic = true;
		_destLeft = new Vector3 (
			leftSide.transform.localPosition.x, 
			leftSide.transform.localPosition.y + distance / 2, 
			leftSide.transform.localPosition.z);

		_destRight = new Vector3 (
			rightSide.transform.localPosition.x, 
			rightSide.transform.localPosition.y - distance / 2, 
			rightSide.transform.localPosition.z);
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.Space)) {
			Debug.Log ("Separate!");
			_breakingUp = true;
		}
	}

	void FixedUpdate () {
		if (_breakingUp) {
			leftSide.transform.localPosition = Vector3.MoveTowards (
				leftSide.transform.localPosition, 
				_destLeft,
				breakUpSpeed);

			rightSide.transform.localPosition = Vector3.MoveTowards (
				rightSide.transform.localPosition, 
				_destRight, 
				breakUpSpeed);
		}
	}
}

