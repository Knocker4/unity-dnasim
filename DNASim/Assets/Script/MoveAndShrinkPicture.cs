﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MoveAndShrinkPicture : MonoBehaviour {

	public GameObject InsertSpiralOrigin;
	public GameObject Matrix;


	private float moveInSpeed = 0.05f;
	private float shrinkSpeed = 0.002f;

	private Boolean _shouldAnimate = false;
	private Boolean _shouldMoveIn = true;

	private Vector3 _dest;
	private Vector3 _targetSize;
	private float _distance = 2.0f;
	private float _size = 2f;

	private long initTime;

	private float timer = 0.0f;
	private float timerMax = 1.0f;

	private float startAlpha = 0.0f;
	private float stepAlpha = 0.005f;
	private float endAlpha = 1.0f;

	// Use this for initialization
	void Start () {
//		_dest = new Vector3(
//			this.transform.position.x,
//			this.transform.position.y,
//			this.transform.position.z
//		);

		_dest = 

		new Vector3(
			InsertSpiralOrigin.transform.localPosition.x,
			InsertSpiralOrigin.transform.localPosition.y + 0.5f,
			InsertSpiralOrigin.transform.localPosition.z
		);

		this.transform.position = new Vector3(
			this.transform.position.x + _distance,
			this.transform.position.y,
			this.transform.position.z
		);
		_targetSize = new Vector3(
			this.transform.localScale.x,
			this.transform.localScale.y,
			0f
		);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			_shouldAnimate = true;
		}
	}

	void FixedUpdate() {
		if (_shouldAnimate) {

			if (_shouldMoveIn && !this.transform.localPosition.Equals (_dest)) {
				this.transform.localPosition = Vector3.MoveTowards (
					this.transform.localPosition, 
					_dest,
					moveInSpeed
				);
			} else {
				_shouldMoveIn = false;
				timer += Time.deltaTime;
				if (timer >= timerMax) {
					//Debug.Log (InsertSpiralOrigin.transform.localPosition);
					if (startAlpha <= endAlpha) {
						startAlpha += stepAlpha;
						this.GetComponent<Renderer> ().material.color = new Color (1.0f, 1.0f, 1.0f, endAlpha - startAlpha);
						Matrix.GetComponent<Renderer> ().material.color = new Color (1.0f, 1.0f, 1.0f, startAlpha);
					} else {
						Matrix.GetComponent<Renderer> ().material.color = new Color (1.0f, 1.0f, 1.0f, endAlpha);
						this.GetComponent<Renderer> ().material.color = new Color (1.0f, 1.0f, 1.0f, endAlpha - startAlpha);
						if (!InsertSpiralOrigin.GetComponent<InsertSpiral> ()._shouldAnimate) {
							InsertSpiralOrigin.GetComponent<InsertSpiral> ().ShouldAnimate (true);
						}
						//this.transform.localPosition = Vector3.MoveTowards (
						//	this.transform.localPosition, 
						//	InsertSpiralOrigin.transform.localPosition,
						//	moveInSpeed
						//);
						this.transform.localScale = Vector3.MoveTowards (
							this.transform.localScale,
							_targetSize,
							shrinkSpeed
						);
					}				
				}
			}

		}

	}

}
